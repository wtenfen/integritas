class ZipUpload < ActiveRecord::Base
  has_attached_file :zip
  validates_attachment_content_type :zip, :content_type => ["application/zip", "application/x-zip", "application/x-zip-compressed"]
  before_post_process :skip_for_zip
  after_commit :process_uploaded_file, on: [:create, :update]

  include ZipUploadAdmin

  def skip_for_zip
     ! %w(application/zip application/x-zip).include?(zip_content_type)
  end

  def process_uploaded_file
    unless processed
      processor= CsvProcessor.new(zip.path)
      processor.process
      if processor.processed
        self.processed=true
        self.warnings=processor.enroll_errors
        save
      end
    end
  end


end
