module ZipUploadAdmin
  extend ActiveSupport::Concern
  def self.included(base)
    
    base.class_eval do

      rails_admin do
        create do 
          field :zip
        end
      end

    end

  end
end