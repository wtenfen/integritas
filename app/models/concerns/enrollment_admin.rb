module EnrollmentAdmin
  extend ActiveSupport::Concern
  def self.included(base)
    
    base.class_eval do

      rails_admin do
        list do
          scopes [:actives, :deleted, :all_admin]
          filters [:course_id]
          
          field :course_id, :enum do
            visible false
            queryable false
            enum do
              Courses::Course.all.pluck(:id, :name).collect{|s| ["#{s[1]}", s[0]] }
            end
          end 
          field :id
          field :student
          field :course do 
            filterable false
          end
          field :status
        end
      end

    end

  end
end