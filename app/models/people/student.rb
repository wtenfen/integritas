module People
  class Student < ActiveRecord::Base
    belongs_to :status, class_name: :'Status'
    has_many   :enrollments, class_name: :'Courses::Enrollment'
    has_many   :courses, class_name: :'Courses::Course', through: :enrollments

    validates_presence_of :status
    
  end
end
