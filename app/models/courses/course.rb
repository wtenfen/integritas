module Courses
  class Course < ActiveRecord::Base
    belongs_to :status,      class_name: :'Status'
    has_many   :enrollments, class_name: :'Courses::Enrollment'
    has_many   :students,    class_name: :'People::Student', through: :enrollments

    validates_presence_of :status

    scope :actives, -> {where(status_id: Status.where(code: 'active').limit(1).pluck(:id).first) }
    scope :deleted, -> {where(status_id: Status.where(code: 'deleted').limit(1).pluck(:id).first) }

  end
end
