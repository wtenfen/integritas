module Courses
  class Enrollment < ActiveRecord::Base
    belongs_to :student,  class_name: :'People::Student'
    belongs_to :course,   class_name: :'Courses::Course'
    belongs_to :status,   class_name: :'Status'

    validates_presence_of :status, :student, :course
    validates_uniqueness_of :course, scope: :student, 
                            message: 'Student already enrolled for this course'

    scope :all_admin, -> { all.includes(:student, :course, :status)}
    scope :actives,   -> { all_admin.joins(:status, :course)
                           .where(status_id: Status.where(code: 'active').limit(1).pluck(:id).first)
                           .merge(Courses::Course.actives) 
                         }
    scope :deleted,   -> { all_admin.joins(:status, :course)
                           .where(status_id: Status.where(code: 'deleted').limit(1).pluck(:id).first)
                           .merge(Courses::Course.actives) 
                         }

    include ::EnrollmentAdmin

  end
end
