require 'zip'
require 'csv'

class ZipFileReader
  attr_reader :zip_path
  attr_accessor :courses, :students, :enrollments

  def initialize(file_path)
    @zip_path= file_path
    @courses= []
    @students= []
    @enrollments= []
  end

  def process
    sort_files_to_array if valid_zip_mime_type?
    self
  end

  private

  def valid_zip_mime_type?
    return false unless File.exist?(zip_path)
    `file  --brief #{zip_path} --mime-type`.gsub(/\n/,"") == "application/zip"
  end

  def sort_files_to_array
    Zip::File.foreach(zip_path) do |zip_file|
      if should_parse_file?(zip_file)
        g=CSV.parse(zip_file.get_input_stream.read, headers:true)
        if g.headers.sort == student_keys.sort
          g.map {|row| @students << row }
        end
        if g.headers.sort == course_keys.sort
          g.map {|row| @courses << row }
        end
        if g.headers.sort == enrollment_keys.sort
          g.map {|row| @enrollments << row }
        end  
      end
    end
  end

  def should_parse_file?(file)
    return false if file.name_is_directory?
    !file.name.include?('__MACOSX')
  end
  
  def student_keys
    ['user_id', 'user_name', 'state']
  end

  def course_keys
    ['course_id', 'course_name', 'state']
  end

  def enrollment_keys
    ['course_id', 'user_id', 'state']
  end

end