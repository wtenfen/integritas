class CsvProcessor
  attr_reader :zip_path, :rows, :active_status, :deleted_status, :enroll_errors, :processed
  delegate :students, :courses, :enrollments, to: :rows

  def initialize(file_path)
    @zip_path=file_path
    @active_status= Status.find_or_create_by(code: 'active', label: 'Active')
    @deleted_status= Status.find_or_create_by(code: 'deleted', label: 'Deleted')
    @new_students=[]
    @new_courses=[]
    @new_enrolls=[]
    @enroll_errors=''
    @processed=false
  end

  def process
   build_students
   build_courses
   build_enrollments
   @processed=true
  end

  private

  def rows
    @rows ||= ZipFileReader.new(zip_path).process
  end

  def build_students
    students.each do |row|
      @new_students << People::Student.find_or_create_by(id: parse_id(row['user_id'])) do |student|
        student.name= row['user_name']
        student.status= parse_status(row['state'])
      end
    end
  end 

  def build_courses
    courses.each do |row|
      @new_courses << Courses::Course.find_or_create_by(id: parse_id(row['course_id'])) do |course|
        course.name= row['course_name']
        course.status= parse_status(row['state'])
      end
    end
  end

  def build_enrollments
    enrollments.each do |row|
      course=Courses::Course.find_by(id: parse_id(row['course_id']))
      student=People::Student.find_by(id: parse_id(row['user_id']))
      if course && student
        @new_enrolls << Courses::Enrollment
         .find_or_create_by(course_id: parse_id(row['course_id']), 
           student_id: parse_id(row['user_id'])) do |enroll|
            enroll.status= parse_status(row['state'])
         end
      else
        @enroll_errors<<"Couldn't import enrollment for Student: #{row['user_id']} and Course: #{row['course_id']}. "
      end
    end
  end

  def parse_id(str)
    str.gsub(/[^0-9]/, '').to_i
  end

  def parse_status(status)
    return active_status if status=='active'
    return deleted_status if status=='deleted'
  end

end