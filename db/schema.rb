# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160501204720) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "courses", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "status_id",              null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "courses", ["status_id"], name: "index_courses_on_status_id", using: :btree

  create_table "enrollments", force: :cascade do |t|
    t.integer  "student_id", null: false
    t.integer  "course_id",  null: false
    t.integer  "status_id",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "enrollments", ["course_id"], name: "index_enrollments_on_course_id", using: :btree
  add_index "enrollments", ["status_id"], name: "index_enrollments_on_status_id", using: :btree
  add_index "enrollments", ["student_id", "course_id"], name: "index_enrollments_on_student_id_and_course_id", unique: true, using: :btree
  add_index "enrollments", ["student_id"], name: "index_enrollments_on_student_id", using: :btree

  create_table "statuses", force: :cascade do |t|
    t.string   "code",       limit: 20,  null: false
    t.string   "label",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "statuses", ["code"], name: "index_statuses_on_code", unique: true, using: :btree

  create_table "students", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "status_id",              null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "students", ["status_id"], name: "index_students_on_status_id", using: :btree

  create_table "zip_uploads", force: :cascade do |t|
    t.text     "warnings"
    t.boolean  "processed"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "zip_file_name"
    t.string   "zip_content_type"
    t.integer  "zip_file_size"
    t.datetime "zip_updated_at"
  end

  add_foreign_key "courses", "statuses"
  add_foreign_key "enrollments", "courses"
  add_foreign_key "enrollments", "statuses"
  add_foreign_key "enrollments", "students"
  add_foreign_key "students", "statuses"
end
