class CreateStatuses < ActiveRecord::Migration
  def change
    create_table :statuses do |t|
      t.column :code, 'char(20)', null: false, index: {unique: true} 
      t.string :label, limit: 255

      t.timestamps null: false
    end
  end
end
