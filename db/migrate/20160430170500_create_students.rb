class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name, limit: 255
      t.references :status, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
