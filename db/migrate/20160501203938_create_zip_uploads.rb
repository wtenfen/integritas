class CreateZipUploads < ActiveRecord::Migration
  def change
    create_table :zip_uploads do |t|
      t.text :warnings
      t.boolean :processed

      t.timestamps null: false
    end
  end
end
