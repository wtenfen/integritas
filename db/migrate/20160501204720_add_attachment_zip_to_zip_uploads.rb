class AddAttachmentZipToZipUploads < ActiveRecord::Migration
  def self.up
    change_table :zip_uploads do |t|
      t.attachment :zip
    end
  end

  def self.down
    remove_attachment :zip_uploads, :zip
  end
end
