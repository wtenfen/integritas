class CreateEnrollments < ActiveRecord::Migration
  def change
    create_table :enrollments do |t|
      t.references :student, index: true, foreign_key: true, null: false
      t.references :course, index: true, foreign_key: true, null: false
      t.references :status, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
    add_index(:enrollments, [:student_id, :course_id], unique: true)
  end
end
