def seed_message(message)
  puts message
end

seed_message 'Creating Statuses...'
  
  statuses=[{code: 'active', label: 'Active'}, {code: 'deleted', label: 'Deleted'} ]
  statuses.each do |status|
    Status.find_or_create_by code: status[:code] do |st|
      st.label= status[:label]
    end 
  end