To run:
 - Ruby 2.2.3 (i'm using with rvm)
 - gem install bundler
 - bundle install
 - rake db:setup
 - RAILS_ENV=test rake db:setup
 - To run specs: rspec spec/  (from project root folder)