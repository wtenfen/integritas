require 'rails_helper'

module People
  RSpec.describe Student, type: :model do
    describe 'factories' do
      it 'has an active Studend factory' do
        student= create(:student, :active)
        expect(student).to be_valid
        expect(student.status.code).to be_eql('active')
      end
    end
    describe 'validations' do 
      it { expect(subject).to validate_presence_of(:status) }
      
    end
  end
end
