require 'rails_helper'

module Courses
  RSpec.describe Course, type: :model do
    it 'has an active Course factory' do
      course= create(:course, :active)
      expect(course).to be_valid
      expect(course.status.code).to be_eql('active')
    end
    describe 'validations' do 
      it { expect(subject).to validate_presence_of(:status) }
    end
  end
end
