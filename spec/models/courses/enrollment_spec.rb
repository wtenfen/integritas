require 'rails_helper'

module Courses
  RSpec.describe Enrollment, type: :model do
    it 'has an active Enrollment factory' do
      enrollment= create(:enrollment, :active)
      expect(enrollment).to be_valid
      expect(enrollment.status.code).to be_eql('active')
    end
    describe 'validations' do 
      it { expect(subject).to validate_presence_of(:status) }
      it { expect(subject).to validate_presence_of(:student) }
      it { expect(subject).to validate_presence_of(:course) }
    end
  end
end
