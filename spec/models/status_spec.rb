require 'rails_helper'

RSpec.describe Status, type: :model do
  describe 'factories' do
    it 'has a valid Active status factory' do
      status= Status.find_by(code: :active)
      expect(status).to be_valid
      expect(status.code).to be_eql('active')
    end
  end
  describe 'validations' do 
    it { expect(subject).to validate_presence_of(:code) }
    it { expect(subject).to validate_uniqueness_of(:code).case_insensitive }
  end
end
