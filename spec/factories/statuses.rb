FactoryGirl.define do
  factory :status do
    trait :active do 
      code 'active'
      label 'Active'
    end
  end
end
