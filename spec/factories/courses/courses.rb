FactoryGirl.define do
  factory :course, :class=> :'Courses::Course' do
    trait :active do
      name 'Advanced Ruby'
      after(:build) {|course| course.status= Status.find_by(code: :active) }
    end
  end
end
