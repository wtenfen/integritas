FactoryGirl.define do
  factory :enrollment, :class => :'Courses::Enrollment' do
    trait :active do
      association :student, :factory  => [:student, :active]
      association :course,  :factory  => [:course, :active]
      after(:build) {|enrollment| enrollment.status= Status.find_by(code: :active) }
    end
  end
end
