FactoryGirl.define do
  factory :student, :class => :'People::Student' do
    trait :active do 
      name 'John Doe'
      after(:build) {|student| student.status= Status.find_by(code: :active) }
    end
  end
end
